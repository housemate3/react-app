# Changelog

All noteable changes will be documented here

## [Unreleased]

##[1.0.0] - 2022-01-06

### Added

- Initialized Create React App (may need to revisit and make it a progressive web app )
- Added Auth0 component to allow for simple log in through gmail or email/password
- After logging in, presents you to a barebones profile page that presents meta data about the user. Also allows for logging out.
- Also upon apprival of the profile page, we call our private api verivying our Auth0 middlewear is working

### Next steps

- Strip away unnecessary code
- Style the profile page (integrate scss)
- After Auth0 login navigate to a sign up flow for the application (relies on backend route)

##[1.1.0] - 2022-03-02

### Added

- sass
- react router
- redux
- modal component
- login / signup flow
