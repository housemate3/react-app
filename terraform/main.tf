module "aws_hosted_website" {
  source = "github.com/rohitnsaigal/terraform-aws-website"

  site_domain     = var.site_domain
  route53_zone_id = var.route53_zone_id
}

output "s3_bucket_for_my_new_website"{
    value=module.aws_hosted_website.s3_bucket
}