export default function deleteAlert({ alertId }) {
  return async (dispatch) => {
    dispatch({ type: 'DELETE_ALERT', alertId })
  }
}
