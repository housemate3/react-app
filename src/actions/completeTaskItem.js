import api from '../api'
import createAlert from '../createAlert'

export default function completeTaskItem({ token, sub, houseId, taskItemId }) {
  return async (dispatch) => {
    try {
      await api.completeTaskItem({ token, sub, houseId, taskItemId })
      dispatch({ type: 'TASK_ITEM_COMPLETED', taskItemId, houseId })
      return true
    } catch (error) {
      dispatch({
        type: 'ADD_ALERT',
        alert: createAlert({ message: error.message, type: 'error' }),
      })
      return false
    }
  }
}
