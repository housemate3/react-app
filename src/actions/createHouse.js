import api from '../api'
import createAlert from '../createAlert'

export default function createHouse({ token, sub, houseName }) {
  return async (dispatch) => {
    try {
      await api.createHouse({ token, sub, houseName })
      dispatch({ type: 'HOUSE_CREATED', house: { houseName, admin: sub } })
      return true
    } catch (error) {
      dispatch({
        type: 'ADD_ALERT',
        alert: createAlert({ message: error.message, type: 'error' }),
      })
      return false
    }
  }
}
