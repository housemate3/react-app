const users = (
  state = {
    houses: [],
  },
  action,
) => {
  switch (action.type) {
    case 'ALL_HOUSES_UPLOADED': {
      return Object.assign({}, state, { houses: action.houses })
    }

    default: {
      return state
    }
  }
}

export default users
