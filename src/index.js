import React from 'react'
import ReactDOM from 'react-dom'
import './sass/index.scss'

import App from './containers/App'
import Alerts from './containers/Alerts.js'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reportWebVitals from './reportWebVitals'
import { Auth0Provider } from '@auth0/auth0-react'
import Auth0Config from './config.json'
import rootReducer from './reducers'
import thunkMiddleware from 'redux-thunk'

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
  ),
)
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Alerts />
      <Auth0Provider {...Auth0Config} redirectUri={window.location.origin}>
        <App />
      </Auth0Provider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
