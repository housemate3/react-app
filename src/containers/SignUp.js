import { connect } from 'react-redux'
import createUser from '../actions/createUser'
import getUser from '../actions/getUser'
import SignUp from '../pages/SignUp.jsx'

const mapStateToProps = (state) => {
  const { token, sub } = state.app
  const { user } = state.user
  return {
    token,
    sub,
    user,
  }
}

const mapDispatchToProps = (dispatch) => ({
  createUser: ({ token, sub, username, profilePicture }) =>
    dispatch(createUser({ token, sub, username, profilePicture })),
  getUser: ({ token, sub }) => dispatch(getUser({ token, sub })),
})

const retVal = connect(mapStateToProps, mapDispatchToProps)(SignUp)

export default retVal
