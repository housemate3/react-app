import { v4 as uuid } from 'uuid'

const createAlert = ({ message, type }) => ({ message, type, alertId: uuid() })

export default createAlert
